package com.kabisa.quotesapi.acceptance.repositories;

import com.kabisa.quotesapi.repositories.QuoteRepository;
import com.kabisa.quotesapi.service.UserService;
import com.kabisa.quotesapi.utils.DatabaseMock;
import net.minidev.json.parser.ParseException;
import org.junit.FixMethodOrder;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@AutoConfigureMockMvc
@SpringBootTest
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
class UserServiceTest {

    @Autowired
    private QuoteRepository quoteRepository;

    @Autowired
    private DatabaseMock databaseMock;

    @Autowired
    private UserService userService;

    @BeforeAll
    public void beforeAll() throws ParseException {
        databaseMock.populateDB();
    }

    @Test
    public void likeQuote() throws ParseException {
        userService.likeQuote("andre", 29);
        Assertions.assertEquals(4, quoteRepository.findByExternalId(29).get().getLikes());
    }

    @Test
    public void userTriesToAddLikeToAlreadyLikedQuote() throws ParseException {
        userService.likeQuote("sarah", 28);
        Assertions.assertEquals(2, quoteRepository.findByExternalId(28).get().getLikes());
    }
}
