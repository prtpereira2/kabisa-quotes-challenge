package com.kabisa.quotesapi.acceptance.controllers;

import com.kabisa.quotesapi.repositories.QuoteRepository;
import com.kabisa.quotesapi.utils.AuthenticationMock;
import com.kabisa.quotesapi.utils.DatabaseMock;
import com.kabisa.quotesapi.repositories.UserRepository;
import net.minidev.json.parser.ParseException;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.*;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@AutoConfigureMockMvc
@SpringBootTest
class UserControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private DatabaseMock databaseMock;

    @Mock
    private static UserRepository userRepository;

    @Mock
    private static QuoteRepository quoteRepository;

    private static String jwt;

    private final static String LIST_USER_ENDPOINT = "/user";
    private final static String LIST_USERS_ENDPOINT = "/users";
    private final static String LIST_USERS_SUGGESTIONS_ENDPOINT = "/users/suggestions";

    @BeforeAll
    public static void BeforeAll() {
        jwt = AuthenticationMock.login();
    }

    @BeforeEach
    public void BeforeEach() throws ParseException {
        databaseMock.populateDB();
    }

    @AfterAll
    public static void AfterAll() {
        userRepository.deleteAll();
        quoteRepository.deleteAll();
    }

    @Test
    public void listLoggedUser() throws Exception {
        mockMvc.perform(
            MockMvcRequestBuilders
                .get(LIST_USER_ENDPOINT)
                .header(HttpHeaders.AUTHORIZATION, "Bearer %s".formatted(jwt))
                .contentType(MediaType.APPLICATION_JSON)
        )
            .andExpect(MockMvcResultMatchers.status().isOk())
            .andExpect(MockMvcResultMatchers.jsonPath("$.username", Matchers.is("andre")));
    }

    @Test
    public void listUsers() throws Exception {
        mockMvc.perform(
            MockMvcRequestBuilders
                .get(LIST_USERS_ENDPOINT)
                .header(HttpHeaders.AUTHORIZATION, "Bearer %s".formatted(jwt))
                .contentType(MediaType.APPLICATION_JSON)
        )
            .andExpect(MockMvcResultMatchers.status().isOk())
            .andExpect(MockMvcResultMatchers.jsonPath("$", Matchers.hasSize(4)))
            .andExpect(MockMvcResultMatchers.jsonPath("$.[0].name", Matchers.is("Andre Pereira")));
    }

    @Test
    public void listUserSuggestions() throws Exception {
        mockMvc.perform(
            MockMvcRequestBuilders
                .get(LIST_USERS_SUGGESTIONS_ENDPOINT)
                .header(HttpHeaders.AUTHORIZATION, "Bearer %s".formatted(jwt))
                .contentType(MediaType.APPLICATION_JSON)
        )
            .andExpect(MockMvcResultMatchers.status().isOk())
            .andExpect(MockMvcResultMatchers.jsonPath("$", Matchers.hasSize(2)))
            .andExpect(MockMvcResultMatchers.jsonPath("$.[0].username", Matchers.is("sarah")))
            .andExpect(MockMvcResultMatchers.jsonPath("$.[1].username", Matchers.is("harold")));
    }
}
