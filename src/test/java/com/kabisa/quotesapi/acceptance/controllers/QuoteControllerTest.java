package com.kabisa.quotesapi.acceptance.controllers;

import com.kabisa.quotesapi.repositories.QuoteRepository;
import com.kabisa.quotesapi.repositories.UserRepository;
import com.kabisa.quotesapi.utils.AuthenticationMock;
import com.kabisa.quotesapi.utils.DatabaseMock;
import net.minidev.json.parser.ParseException;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@AutoConfigureMockMvc
@SpringBootTest
class QuoteControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private DatabaseMock databaseMock;

    @Mock
    private static UserRepository userRepository;

    @Mock
    private static QuoteRepository quoteRepository;

    private static String jwt;

    private final static String LIKE_QUOTE_5_ENDPOINT = "/quote/5/like";
    private final static String GET_RANDOM_QUOTE_ENDPOINT = "/quote";

    @BeforeAll
    public static void BeforeAll() {
        jwt = AuthenticationMock.login();
    }

    @BeforeEach
    public void BeforeEach() throws ParseException {
        databaseMock.populateDB();
    }

    @AfterAll
    public static void AfterAll() {
        userRepository.deleteAll();
        quoteRepository.deleteAll();
    }

    @Test
    public void addLikeToQuote() throws Exception {
        mockMvc.perform(
            MockMvcRequestBuilders
                .post(LIKE_QUOTE_5_ENDPOINT)
                .header(HttpHeaders.AUTHORIZATION, "Bearer %s".formatted(jwt))
                .contentType(MediaType.APPLICATION_JSON)
        )
            .andExpect(MockMvcResultMatchers.status().isOk())
            .andExpect(MockMvcResultMatchers.jsonPath("$.externalId", Matchers.is(5)))
            .andExpect(MockMvcResultMatchers.jsonPath("$.likes", Matchers.is(1)));
    }

    @Test
    public void fetchRandomQuote() throws Exception {
        mockMvc.perform(
            MockMvcRequestBuilders
                .get(GET_RANDOM_QUOTE_ENDPOINT)
                .header(HttpHeaders.AUTHORIZATION, "Bearer %s".formatted(jwt))
                .contentType(MediaType.APPLICATION_JSON)
        )
            .andExpect(MockMvcResultMatchers.status().isOk());
    }
}
