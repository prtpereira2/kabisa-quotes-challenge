package com.kabisa.quotesapi.repositories;

import com.kabisa.quotesapi.models.User;
import com.kabisa.quotesapi.representers.UserOutputRepresenter;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import java.util.List;
import java.util.UUID;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    User findByUsername(String username);

    @Query( value =
            "SELECT name, username FROM ( " +
                "SELECT DISTINCT user_id FROM users_liked_quotes " +
                "WHERE user_id <> :user_id AND liked_quotes_id IN :liked_quotes) as ulq " +
            "INNER JOIN users ON users.id = ulq.user_id",
            nativeQuery=true
    )
    List<UserOutputRepresenter> findSuggestedUsers(@Param("user_id") UUID userId, @Param("liked_quotes") List<UUID> likedQuotes);
}