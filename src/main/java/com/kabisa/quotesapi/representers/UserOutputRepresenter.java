package com.kabisa.quotesapi.representers;

public interface UserOutputRepresenter {
    String getName();
    String getUsername();
}
