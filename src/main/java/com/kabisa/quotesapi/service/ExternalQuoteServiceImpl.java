package com.kabisa.quotesapi.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.minidev.json.JSONObject;
import net.minidev.json.parser.JSONParser;
import net.minidev.json.parser.ParseException;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import javax.transaction.Transactional;

@Service
@RequiredArgsConstructor
@Transactional
@Slf4j
public class ExternalQuoteServiceImpl implements ExternalQuoteService {
    public static final String GET_RANDOM_QUOTE_ENDPOINT = "http://quotes.stormconsultancy.co.uk/random.json";

    private final RestTemplate restTemplate = new RestTemplate();
    private final JSONParser parser = new JSONParser();

    @Override
    public JSONObject generateRandomQuote() throws ParseException {
        String result = restTemplate.getForObject(GET_RANDOM_QUOTE_ENDPOINT, String.class);

        JSONObject externalQuote = (JSONObject) parser.parse(result);
        return externalQuote;
    }

    @Override
    public JSONObject getQuoteById(long id) throws ParseException {
        String endpoint = "http://quotes.stormconsultancy.co.uk/quotes/%s.json".formatted(id);
        String result = restTemplate.getForObject(endpoint, String.class);

        JSONObject externalQuote = (JSONObject) parser.parse(result);
        return externalQuote;
    }
}
