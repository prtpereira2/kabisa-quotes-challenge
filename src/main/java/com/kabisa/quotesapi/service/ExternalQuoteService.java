package com.kabisa.quotesapi.service;

import net.minidev.json.JSONObject;
import net.minidev.json.parser.ParseException;

public interface ExternalQuoteService {

    JSONObject generateRandomQuote() throws ParseException;
    JSONObject getQuoteById(long id) throws ParseException;
}
