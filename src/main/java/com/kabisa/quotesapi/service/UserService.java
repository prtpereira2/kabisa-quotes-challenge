package com.kabisa.quotesapi.service;

import com.kabisa.quotesapi.models.Quote;
import com.kabisa.quotesapi.models.Role;
import com.kabisa.quotesapi.models.User;
import com.kabisa.quotesapi.representers.UserOutputRepresenter;
import net.minidev.json.parser.ParseException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface UserService{

    User getUser(String username);
    Optional<User> saveUser(User user);
    List<User> getUsers();
    List<UserOutputRepresenter> getSuggestedUsers(String username);
    Optional<Role> saveRole(Role role);
    Optional<Role> addRoleToUser(String username, String roleName);
    Quote saveQuote(Quote quote);
    Optional<Quote> likeQuote(String username, UUID id) throws ParseException;
    Optional<Quote> likeQuote(String username, long externalId) throws ParseException;
    Page<Quote> listQuotes(Pageable page);
}
