package com.kabisa.quotesapi.service;

import com.kabisa.quotesapi.models.Quote;
import com.kabisa.quotesapi.models.Role;
import com.kabisa.quotesapi.models.User;
import com.kabisa.quotesapi.repositories.QuoteRepository;
import com.kabisa.quotesapi.repositories.RoleRepository;
import com.kabisa.quotesapi.repositories.UserRepository;
import com.kabisa.quotesapi.representers.UserOutputRepresenter;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.minidev.json.JSONObject;
import net.minidev.json.parser.ParseException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
@RequiredArgsConstructor @Transactional @Slf4j
public class UserServiceImpl implements UserService, UserDetailsService {
    private final UserRepository userRepository;
    private final RoleRepository roleRepository;
    private final QuoteRepository quoteRepository;
    private final PasswordEncoder passwordEncoder;
    private final ExternalQuoteService externalQuoteService;

    @Override
    public User getUser(String username) {
        User user = userRepository.findByUsername(username);
        if (user == null) {
            log.error("User not found in the database");
            throw new UsernameNotFoundException("User not found in the database");
        } else {
            return user;
        }
    }

    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = getUser(username);

        Collection<SimpleGrantedAuthority> authorities = new ArrayList<>();
        user.getRoles().forEach(role -> {
            authorities.add(new SimpleGrantedAuthority(role.getName()));
        });
        return new org.springframework.security.core.userdetails.User(user.getUsername(), user.getPassword(), authorities);
    }

    @Override
    public Optional<User> saveUser(User user) {
        User existentUser = userRepository.findByUsername(user.getUsername());
        log.info("Saving new user", user.getName());

        if (existentUser == null) {
            user.setPassword(passwordEncoder.encode(user.getPassword()));
            return Optional.of(userRepository.save(user));
        } else {
            log.error("Username '{}' is already taken.", user.getUsername());
            return Optional.empty();
        }
    }

    @Override
    public List<User> getUsers() {
        return userRepository.findAll();
    }

    @Override
    public List<UserOutputRepresenter> getSuggestedUsers(String username) {
        User user = getUser(username);
        Collection<Quote> quotes = user.getLikedQuotes();

        log.info("watson 1");
        List<UserOutputRepresenter> suggested = userRepository.findSuggestedUsers(user.getId(), quotes.stream().map(Quote::getId).toList());
        log.info("watson 4");
        return suggested;
    }

    @Override
    public Optional<Role> addRoleToUser(String username, String roleName) {
        User user = userRepository.findByUsername(username);
        Role role = roleRepository.findByName(roleName);

        if (user != null && role!= null && !user.getRoles().contains(role)) {
            user.getRoles().add(role);
            return Optional.of(role);
        } else {
            return Optional.empty();
        }
    }

    @Override
    public Optional<Role> saveRole(Role role) {
        Role existentRole = roleRepository.findByName(role.getName());

        if (existentRole == null) {
            log.info("Saving new role", role.getName());
            return Optional.of(roleRepository.save(role));
        } else {
            log.error("Role '{}' already exists.", role.getName());
            return Optional.empty();
        }
    }

    @Override
    public Quote saveQuote(Quote quote) {
        Quote existentQuote = quoteRepository.findByExternalId(quote.getExternalId()).orElse(null);

        if (existentQuote == null) {
            return quoteRepository.save(quote);
        } else {
            log.error("Quote already exists.");
            return existentQuote;
        }
    }

    @Override
    public Optional<Quote> likeQuote(String username, UUID id) throws ParseException {
        Optional<Quote> quote = quoteRepository.findById(id);

        if (quote.isPresent()) {
            return likeQuote(username, quote.get().getExternalId());
        } else {
            return quote;
        }
    }

    @Override
    public Optional<Quote> likeQuote(String username, long externalId) throws ParseException {
        Quote quote = quoteRepository.findByExternalId(externalId).orElse(null);
        User user = getUser(username);

        if (quote == null) {
            JSONObject externalQuote = externalQuoteService.getQuoteById(externalId);
            quote = saveQuote(
                new Quote(null, externalQuote.getAsNumber("id").longValue())
            );
        }

        Collection<Quote> quotes = user.getLikedQuotes();

        if (quotes.stream().filter( q -> q.getExternalId() == externalId).findAny().isEmpty()) {
            user.getLikedQuotes().add(quote);
            quote.incrementLike();
        } else {
            log.info("User {} already liked the quote", username);
        }

        return Optional.of(quote);
    }

    @Override
    public Page<Quote> listQuotes(Pageable page) {
        return quoteRepository.findAll(page);
    }
}
