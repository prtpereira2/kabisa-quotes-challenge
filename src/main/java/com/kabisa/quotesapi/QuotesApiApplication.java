package com.kabisa.quotesapi;

import com.kabisa.quotesapi.models.Role;
import com.kabisa.quotesapi.models.User;
import com.kabisa.quotesapi.service.UserService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.ArrayList;

@SpringBootApplication
public class QuotesApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(QuotesApiApplication.class, args);
    }

    @Bean
    PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    // For local testing purposes only

    @Bean
    CommandLineRunner run(UserService userService) {
        return args -> {
            userService.saveRole((new Role(null, "ROLE_USER")));
            userService.saveRole((new Role(null, "ROLE_ADMIN")));

            userService.saveUser(new User(null, "Andre Pereira", "andre", "123", new ArrayList<>()));
            userService.saveUser(new User(null, "John Doe", "john", "123", new ArrayList<>()));
            userService.saveUser(new User(null, "Sarah Connor", "sarah", "123", new ArrayList<>()));
            userService.saveUser(new User(null, "Harold Finch", "harold", "123", new ArrayList<>()));

            userService.addRoleToUser("andre", "ROLE_USER");
            userService.addRoleToUser("andre", "ROLE_ADMIN");
            userService.addRoleToUser("john", "ROLE_USER");
            userService.addRoleToUser("sarah", "ROLE_USER");
            userService.addRoleToUser("harold", "ROLE_USER");

            userService.likeQuote("andre", 27);
            userService.likeQuote("andre", 28);

            userService.likeQuote("john", 29);
            userService.likeQuote("john", 30);
            userService.likeQuote("john", 31);

            userService.likeQuote("sarah", 28);
            userService.likeQuote("sarah", 29);

            userService.likeQuote("harold", 27);
            userService.likeQuote("harold", 29);
        };
    }


}
