package com.kabisa.quotesapi.controllers;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.kabisa.quotesapi.models.Quote;
import com.kabisa.quotesapi.service.ExternalQuoteService;
import com.kabisa.quotesapi.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.minidev.json.JSONObject;
import net.minidev.json.parser.ParseException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import javax.servlet.http.HttpServletRequest;
import java.util.Optional;

@RestController
@RequiredArgsConstructor
@Slf4j
public class QuoteController {
    private final ExternalQuoteService externalQuoteService;
    private final UserService userService;

    @GetMapping("/quote")
    public ResponseEntity<JSONObject> getQuote() throws ParseException {
        JSONObject json = externalQuoteService.generateRandomQuote();
        return ResponseEntity.ok().body(json);
    }

    @GetMapping("/quotes")
    public ResponseEntity<Page<Quote>> listQuotes(Pageable page) {
        Page<Quote> quotes = userService.listQuotes(page);
        return ResponseEntity.ok().body(quotes);
    }

    @PostMapping("/quote/{quote_id}/like")
    public ResponseEntity<Optional<Quote>> likeQuote(HttpServletRequest request, @PathVariable long quote_id) throws ParseException {
        String authorizationHeader = request.getHeader("Authorization");
        if(authorizationHeader != null && authorizationHeader.startsWith("Bearer ")) {
            try {
                String refreshToken = authorizationHeader.substring("Bearer ".length());
                Algorithm algorithm = Algorithm.HMAC256("secret".getBytes());
                JWTVerifier verifier = JWT.require(algorithm).build();
                DecodedJWT decodedJWT = verifier.verify((refreshToken));
                String username = decodedJWT.getSubject();

                Optional<Quote> quote = userService.likeQuote(username, quote_id);
                return ResponseEntity.ok().body(quote);
            }
            catch (Exception exception){
                log.error("Error on like operation. Exception: {}", exception.getMessage());
                return ResponseEntity.internalServerError().body(null);
            }
        } else {
            throw new RuntimeException("Access_token is missing");
        }
    }
}
