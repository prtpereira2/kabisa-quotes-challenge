package com.kabisa.quotesapi.controllers;

import com.kabisa.quotesapi.models.Role;
import com.kabisa.quotesapi.models.User;
import com.kabisa.quotesapi.representers.RoleToUserInputRepresenter;
import com.kabisa.quotesapi.representers.UserOutputRepresenter;
import com.kabisa.quotesapi.security.GetAuthenticatedUserInteractor;
import com.kabisa.quotesapi.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.minidev.json.JSONObject;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import javax.servlet.http.HttpServletRequest;
import java.net.URI;
import java.util.List;
import java.util.Optional;

@RestController
@RequiredArgsConstructor
@Slf4j
public class UserController {
    private final GetAuthenticatedUserInteractor getAuthenticatedUserInteractor;
    private final UserService userService;

    @GetMapping("/user")
    public ResponseEntity<User> getUser(HttpServletRequest request) {
        try {
            User user = getAuthenticatedUserInteractor.call(request);
            if (user == null) {
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
            } else {
                return ResponseEntity.ok().body(user);
            }
        } catch (Exception ex) {
            return ResponseEntity.internalServerError().body(null);
        }
    }

    @PostMapping("/user")
    public ResponseEntity<Optional<User>> addUser(@RequestBody User user) {
        URI uri = URI.create(ServletUriComponentsBuilder.fromCurrentContextPath().path("/user").toUriString());
        Optional<User> savedUser = userService.saveUser(user);
        if (savedUser.isPresent()) {
            return ResponseEntity.created(uri).body(savedUser);
        } else {
            JSONObject conflictMessage = new JSONObject();
            conflictMessage.put("error_code", 409);
            conflictMessage.put("error_message", "Username already taken.");
            return new ResponseEntity(conflictMessage, HttpStatus.CONFLICT);
        }
    }

    @GetMapping("/users")
    public ResponseEntity<List<User>> getUsers() {
        return ResponseEntity.ok().body(userService.getUsers());
    }

    @GetMapping("/users/suggestions")
    public ResponseEntity<List<UserOutputRepresenter>> getSuggestedUsers(HttpServletRequest request) {
        try {
            User user = getAuthenticatedUserInteractor.call(request);
            List<UserOutputRepresenter> suggestedUsers = userService.getSuggestedUsers(user.getUsername());
            return ResponseEntity.ok().body(suggestedUsers);
        } catch (Exception exception) {
            log.error(exception.toString());
            return ResponseEntity.internalServerError().body(null);
        }
    }

    @PostMapping("/role")
    public ResponseEntity<Optional<Role>> addRole(@RequestBody Role role) {
        URI uri = URI.create(ServletUriComponentsBuilder.fromCurrentContextPath().path("/role").toUriString());
        Optional<Role> addedRole = userService.saveRole(role);

        if (addedRole.isPresent()) {
            return ResponseEntity.created(uri).body(addedRole);
        } else {
            JSONObject conflictMessage = new JSONObject();
            conflictMessage.put("error_code", 409);
            conflictMessage.put("error_message", "Role already exists.");
            return new ResponseEntity(conflictMessage, HttpStatus.CONFLICT);
        }
    }

    @PostMapping("/user/role")
    public ResponseEntity<?> addRoleTouser(@RequestBody RoleToUserInputRepresenter roleToUserInput) {
        URI uri = URI.create(ServletUriComponentsBuilder.fromCurrentContextPath().path("/user/role").toUriString());

        Optional<Role> linkedRole = userService.addRoleToUser(roleToUserInput.getUsername(), roleToUserInput.getRoleName());
        if (linkedRole.isPresent()) {
            return ResponseEntity.created(uri).body(linkedRole);
        } else {
            JSONObject conflictMessage = new JSONObject();
            conflictMessage.put("error_code", 409);
            conflictMessage.put("error_message", "User or role does not exist.");
            return new ResponseEntity(conflictMessage, HttpStatus.CONFLICT);
        }
    }
}
