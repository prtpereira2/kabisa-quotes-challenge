package com.kabisa.quotesapi.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.annotations.Type;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import java.util.ArrayList;
import java.util.Collection;
import java.util.UUID;

import static javax.persistence.FetchType.EAGER;
import static javax.persistence.GenerationType.AUTO;

@Entity
@Table(name="users")
public class User {
    @Id
    @Type(type="pg-uuid")
    @GeneratedValue(strategy = AUTO)
    private UUID id;
    private String name;
    @Column(unique=true)
    private String username;
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private String password;
    @ManyToMany()
    private Collection<Quote> likedQuotes = new ArrayList<>();
    @ManyToMany(fetch = EAGER)
    private Collection<Role> roles = new ArrayList<>();

    public User() {
        super();
    }

    public User(UUID id, String name, String username, String password, Collection<Quote> likedQuotes, Collection<Role> roles) {
        this.id = id;
        this.name = name;
        this.username = username;
        this.password = password;
        this.likedQuotes = likedQuotes;
        this.roles = roles;
    }

    public User(UUID id, String name, String username, String password, Collection<Role> roles) {
        this.id = id;
        this.name = name;
        this.username = username;
        this.password = password;
        this.likedQuotes = new ArrayList<>();
        this.roles = roles;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Collection<Quote> getLikedQuotes() {
        return likedQuotes;
    }

    public Collection<Role> getRoles() {
        return roles;
    }

    public void setLikedQuotes(Collection<Quote> likedQuotes) {
        this.likedQuotes = likedQuotes;
    }

    public void setRoles(Collection<Role> roles) {
        this.roles = roles;
    }
}
