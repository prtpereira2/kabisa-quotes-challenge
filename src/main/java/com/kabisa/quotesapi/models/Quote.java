package com.kabisa.quotesapi.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.UUID;
import static javax.persistence.GenerationType.AUTO;

@Entity
public class Quote {
    @Id
    @GeneratedValue(strategy = AUTO)
    private UUID id;
    private long externalId;
    private long likes;

    public Quote(UUID id, long externalId, long likes) {
        this.id = id;
        this.externalId = externalId;
        this.likes = likes;
    }

    public Quote(UUID id, long externalId) {
        this.id = id;
        this.externalId = externalId;
        this.likes = 0;
    }

    public Quote() {
        super();
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public long getExternalId() {
        return externalId;
    }

    public void setExternalId(long externalId) {
        this.externalId = externalId;
    }

    public long getLikes() {
        return likes;
    }

    public void setLikes(long likes) {
        this.likes = likes;
    }

    public void incrementLike() {
        this.likes += 1;
    }
}
