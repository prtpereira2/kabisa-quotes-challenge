# Kabisa - Quotes challenge



## Getting started

This application follows the Kabisa challenge.

It was created a service that is responsible to manage users and quotes that are fetched from another service.

In other means, this is an API, whose documentation can be checked by [opening this HTML file](./documentation/quotes-api-documentation-swagger.html) in your preferred browser.

## Start the application

#### Running the app

```
 docker-compose build
 docker-compose up app
```

The above commands, will start build a docker container, starting the web service, on localhost, port 8081.

#### Supported endpoints

Please check [this documentation](./documentation/quotes-api-documentation-swagger.html) to see all endpoints this service supports.

#### Calling the API

Before using the API, you must be authenticated into the service. To do that, you need to call the `/api/login` endpoint, providing the `username` and `password` fields.

Example:
```
POST http://localhost:8081/api/login

application/x-www-form-urlencoded
 - "username": "andre"
 - "password": "123"
}
```

The service will respond you an `access_token` and a `refresh_token`. You just need to grab the `access_token` and and the following configuration to the request header:

```
Authorization: Bearer <your_access_token>
```

The application already starts with some populated users into the service. So you can login into the already created users or create your own user for yourself. You just have to call the endpoint responsible to create a new user.

Example:
```
POST http://localhost:8081/user
Header: 
  Authorization: Bearer <your_access_token>
  
application/json
{
    "name": "John Doe",
    "username": "john_doe",
    "password": "this_is_my_password"
}
```


If you want to call the endpoint responsible to return a random quote, you can perform the following request:

```
GET http://localhost:8081/quote
Header: 
  Authorization: Bearer <your_access_token>
```

#### Pageable endpoint

Get all the quotes sorted by likes (for example), or limit the number of fetched results and search by page.

* GET /quotes
```
GET http://localhost:8081/quotes?sort=likes,desc
Header: 
  Authorization: Bearer <your_access_token>
```

